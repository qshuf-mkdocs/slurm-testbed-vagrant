---
title: "Cleanup"
weight: 4
---

The cleanup of the vagrant setup is rather easy. 

1. destroy the stack 
1. remove the boxes

### Destroy Stack

Log out of the vagrant box until you are in your shell (where the Vagrantfile is located).

```bash
vagrant status
```

This will show the status of the stack

```bash
$ vagrant status
Current machine states:

headnode                  running (virtualbox)
compute1                  not created (virtualbox)
compute2                  not created (virtualbox)
compute3                  not created (virtualbox)
compute4                  not created (virtualbox)
```

Now you destroy the complete stack.

```bash
vagrant destroy -f
```

```bash
$ vagrant destroy -f
==> compute4: VM not created. Moving on...
==> compute3: VM not created. Moving on...
==> compute2: VM not created. Moving on...
==> compute1: VM not created. Moving on...
==> headnode: Forcing shutdown of VM...
==> headnode: Destroying VM and associated drives...
```

### Remove boxes

Check the downloaded boxes with `vagrant box list`.

```bash
$  vagrant box list
qnib/rocky8-slurmctld                                                                (virtualbox, 2022.02.06-v7)
qnib/rocky8-slurmd                                                                (virtualbox, 2022.02.06)
```

And remove the boxes you do not want to keep

```bash
vagrant box remove qnib/rocky8-slurmctld --provider virtualbox --all
vagrant box remove qnib/rocky8-slurmd --provider virtualbox --all
```

That'll remove the `qnib/rocky8-slurmctld` and `qnib/rocky8-slurmd` boxes - **all** of them.

```bash
$ vagrant box remove qnib/rocky8-slurmctld --provider virtualbox --all
Removing box 'qnib/rocky8-slurmctld' (v2022.02.06) with provider 'virtualbox'...
Removing box 'qnib/rocky8-slurmctld' (v2022.02.06-v7) with provider 'virtualbox'...
```
