---
title: "Home"
hide:
- navigation
- toc
---
# Create SLURM testbed using Vagrant
This workshop walks you through the basic setup and usage of a vagrant based Slurm cluster.
We'll spin up one headnode and four small compute nodes to explore Slurm on such a small setup.

### The Setup

We'll going to use [Vagrant]() to create a 5 VM setup. One headnode providing

1. An NFS server (`nfsd`) to provide a shared home and data directory
1. A Slurm server (`slurmctld`) plus a slurm client (`slurmd`)

The headnode has 2 CPUs (be default) and as such will be used as a `build` node. 

The compute nodes will mount the NFS shares and run their own `slurmd` client.

#### Software Stack

All nodes are able to run `spack`, container runtimes and container engines.


#### Diagram

``` mermaid
classDiagram
    headnode <|-- compute1
    headnode <|-- compute2
    headnode <|-- compute3
    headnode <|-- compute4           
    headnode :  nfsd, slurmctld, slurmd
    headnode :  spack, runc,crun, sarus
    compute1 :  slurmd
    compute1 :  spack, runc,crun, sarus
    compute2 :  slurmd
    compute2 :  spack, runc,crun, sarus
    compute3 :  slurmd
    compute3 :  spack, runc,crun, sarus
    compute4 :  slurmd
    compute4 :  spack, runc,crun, sarus
```
