---
title: "Install OpenMPI"
weight: 5
---
To finish the setup we'll install some software.

```{ .bash .annotate }
time spack install openmpi
```



Using the binary cache speeds up the installation. Installing from cache takes a couple of minutes (<5min) while installing from source takes 10 times that (30min with 4 cores).

=== "With Cache"
  
    ```bash
    $ time spack install openmpi
    ==> Bootstrapping clingo from pre-built binaries
    ==> Fetching https://mirror.spack.io/bootstrap/github-actions/v0.1/build_cache/linux-rhel5-x86_64/gcc-9.3.0/clingo-bootstrap-spack/linux-rhel5-x86_64-gcc-9.3.0-clingo-bootstrap-spack-vcipwnf57slgoo7busvvkzjkk7vydeb5.spack
    ==> Installing "clingo-bootstrap@spack%gcc@9.3.0~docs~ipo+python build_type=Release arch=linux-rhel5-x86_64" from a buildcache
    ==> Bootstrapping patchelf from pre-built binaries
    *snip*
    ==> patchelf: Successfully installed patchelf-0.13.1-vwwlq7rporcwpl3v73c42kxqgygktq2d
    Fetch: 3.99s.  Build: 8.12s.  Total: 12.11s.
    [+] /home/vagrant/.spack/bootstrap/store/linux-rocky8-x86_64/gcc-8.5.0/patchelf-0.13.1-vwwlq7rporcwpl3v73c42kxqgygktq2d
    ==> Installing libsigsegv-2.13-h7wzxrhb52yqigmv4akyw4w2dkwx6eqo
    ==> Extracting libsigsegv-2.13-h7wzxrhb52yqigmv4akyw4w2dkwx6eqo from binary cache
    gpg: Signature made Sat 29 Jan 2022 05:42:50 PM UTC
    gpg:                using RSA key 08A5C5675C1F8068AD7CC22B48075C2C6FE1C6ED
    gpg:                issuer "spack@qnib.org"
    gpg: Good signature from "qnib-binary-cache-key (GPG created for Spack) <spack@qnib.org>" [ultimate]
    [+] /nfs/share/spack/pkg/linux-rocky8-x86_64/gcc-8.5.0/libsigsegv-2.13-h7wzxrhb52yqigmv4akyw4w2dkwx6eqo
    *snip*
    ==> Installing openmpi-4.1.2-fppvj6qavqixxzgllgabdwssnapy7knr
    ==> Extracting openmpi-4.1.2-fppvj6qavqixxzgllgabdwssnapy7knr from binary cache
    [+] /nfs/share/spack/pkg/linux-rocky8-x86_64/gcc-8.5.0/openmpi-4.1.2-fppvj6qavqixxzgllgabdwssnapy7knr
    real	3m31.110s
    user	0m22.688s
    sys	0m4.367s
    ```
    !!! note "version pinning"
        The workshop setup so far does not pin the versions of all dependencies for Open MPI. It might happen that certain packages got an update and are going to be rebuild.

=== "Without Cache"

    I removed some noise about how it was installed.
    ```bash
    $ time spack install openmpi
    ==> Installing libsigsegv-2.13-h7wzxrhb52yqigmv4akyw4w2dkwx6eqo
    [+] /nfs/share/spack/pkg/linux-rocky8-x86_64/gcc-8.5.0/libsigsegv-2.13-h7wzxrhb52yqigmv4akyw4w2dkwx6eqo
    ==> Installing pkgconf-1.8.0-nodnfqwsxuesvfr2qjzqbueimsnd2pkb
    [+] /nfs/share/spack/pkg/linux-rocky8-x86_64/gcc-8.5.0/pkgconf-1.8.0-nodnfqwsxuesvfr2qjzqbueimsnd2pkb
    *snip*
    ==> Installing numactl-2.0.14-hlz3v4n6pgxbssonkv7olmju2u5e3uit
    [+] /nfs/share/spack/pkg/linux-rocky8-x86_64/gcc-8.5.0/numactl-2.0.14-hlz3v4n6pgxbssonkv7olmju2u5e3uit
    ==> Installing openmpi-4.1.2-golv7bwj46ali77oy7b633b2eqpgdzdg
    [+] /nfs/share/spack/pkg/linux-rocky8-x86_64/gcc-8.5.0/openmpi-4.1.2-golv7bwj46ali77oy7b633b2eqpgdzdg
    real    25m48.523s
    user    41m51.354s
    sys	    11m6.183s
    ```
