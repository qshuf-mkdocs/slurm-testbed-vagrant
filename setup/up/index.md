---
title: "Start Stack"
weight: 4
---
Ok, now you got it all Virtualbox, Vagrant and the git repository ready to be started up.


### Stack Discovery

The following block goes through the `Vagrantfile` with inline explanation what is going on.

??? note Vagrantfile 

    The Vagrantfile holds two types of nodes (headnode and compute node) and defines IP addresses and hostname and whether they should start automatically when you run `vagrant up`.

    ``` { .yaml .annotate }
    servers=[
    {
        :hostname => "compute1",
        :ip => "192.168.56.11",
        :autostart => true
    },{
        :hostname => "compute2",
        :ip => "192.168.56.12",
        :autostart => true
    },{
        :hostname => "compute3",
        :ip => "192.168.56.13",
        :autostart => false
    },{
        :hostname => "compute4",
        :ip => "192.168.56.14",
        :autostart => false
    }
    ]

    $script = <<SCRIPT
    cat >> /etc/hosts << EOF
    192.168.56.10 headnode
    192.168.56.11 compute1
    192.168.56.12 compute2
    192.168.56.13 compute3
    192.168.56.14 compute4
    EOF
    if [[ "$(hostname)" == "headnode" ]];then
        echo ">> Copy munge.key to NFS share"
        cp -rf /etc/munge/munge.key /nfs/share/
        chmod 444 /nfs/share/munge.key
        su - alice -c '< /dev/zero ssh-keygen -q -N ""'
        su - alice -c 'cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys'
    else 
        echo ">> Wait for /nfs/share/munge.key"
        while [ ! -f /nfs/share/munge.key ]; do 
          echo -n "."
          sleep 1; 
        done
        echo " OK"
        echo ">> Copy munge.key from NFS share"    
        cp -rf /nfs/share/munge.key /etc/munge/munge.key
        chmod 400 /etc/munge/munge.key
    fi
    for svc in munge $@;do
        sleep 2
        echo ">> Restart ${svc}"
        systemctl restart ${svc}
    done
    SCRIPT

    Vagrant.configure("2") do |config|
    config.vm.define "headnode", primary: true do |head| # (1)
        head.vm.box = "qnib/rocky8-slurmctld"
        head.vm.box_version = "2022.03.02"
        head.vbguest.auto_update = false
        head.vm.network "private_network", ip: "192.168.56.10" # (2)
        head.vm.hostname = "headnode"
        head.ssh.username = "alice"
        head.vm.provider "virtualbox" do |vb|
          vb.memory = 3048
          vb.cpus = 4
        end
        head.vm.provision "shell" do |s|
          s.inline = $script
          s.args   = ["slurmctld", "slurmd"] # (3)
        end
    end
    servers.each do |machine| # (4)
        config.vm.define machine[:hostname], autostart:machine[:autostart] do |compute| # (5)
        compute.vm.box = "qnib/rocky8-slurmd"
        compute.vm.box_version = "2022.03.02-v1"
        compute.vbguest.auto_update = false
        compute.vm.hostname = machine[:hostname] # (6)
        compute.vm.network "private_network", ip: machine[:ip]
        compute.ssh.username = "vagrant"
        compute.vm.provider "virtualbox" do |vb|
            vb.memory = 1024
            vb.cpus = 1
        end
        compute.vm.provision "shell" do |s|
            s.inline = $script
            s.args   = ["slurmd"]
          end
        end
      end
    end
    ```
    
    1. Defining the `headnode` as `primary` will connect to the box with a simple `vagrant ssh`.
    2. all nodes will have their own private network; here we define the IP for the headnode
    3. We are going to restart `slurmctld` and `slurmd`
    4. iterate through the JSON define at the top and put the elements into the variable `machine`.
    5. use `hostname` to name the machine and `autostart` to define whether the machine should be started automatically (suprise).
    6. set the hostname and below the IP according to the JSON.

### Vagrant Up

To start the stack, you simple run `vagrant up`, in the stack you just cloned it will start the headnode and the first two computenodes.

```bash
vagrant up
```

That'll download the latest version of the [qnib/rocky8-slurmctld](https://app.vagrantup.com/qnib/boxes/rocky8-slurmctld) and [qnib/rocky8-slurmd](https://app.vagrantup.com/qnib/boxes/rocky8-slurmd) images from Vagrant Cloud and start the machine.

```bash
$ vagrant up
Bringing machine 'headnode' up with 'virtualbox' provider...
Bringing machine 'compute1' up with 'virtualbox' provider...
Bringing machine 'compute2' up with 'virtualbox' provider...
==> headnode: Importing base box 'qnib/rocky8-slurmctld'...
*snip*
==> headnode: Running provisioner: shell...
    headnode: Running: inline script
    headnode: >> Copy munge.key to NFS share
    headnode: Enter file in which to save the key (/nfs/home/alice/.ssh/id_rsa): 
    headnode: >> Restart munge
    headnode: >> Restart slurmctld
    headnode: >> Restart slurmd
==> compute1: Importing base box 'qnib/rocky8-slurmd'...
*snip*
==> compute1: Running provisioner: shell...
    compute1: Running: inline script
    compute1: >> Wait for /nfs/share/munge.key
    compute1: .. OK
    compute1: >> Copy munge.key from NFS share
    compute1: >> Restart munge
    compute1: >> Restart slurmd
==> compute2: Importing base box 'qnib/rocky8-slurmd'
```

### Connect

When using `ssh` to connect to the cluster you are connected to the primary (as specified in the Vagrantfile). 

```bash
vagrant ssh
```

You'll be logged in as `alice`. Alice is part of the `compute` group, has a shared home, can run `sudo` to gain privileged access and is a software admin to install spack packages.

Alice is able to log into compute nodes:

```bash
ssh compute1
```


### Slurm 101

Once logged in we can run Slurm commands. `sinfo` to list all partitions and `srun` to run a command like `hostname`.

```{ .bash .no-copy}
$ sinfo
PARTITION AVAIL  TIMELIMIT  NODES  STATE NODELIST
headnode     up      15:00      1   idle headnode
compute*     up   infinite      2   unk* compute[3-4]
compute*     up   infinite      2   idle compute[1-2]
$ srun hostname
compute1
$
```
