---
title: "Vagrant"
weight: 2
---
Vagrant is the swiss army knive for virtualizes stack. Personally it was my precursor to docker-compose in the container ecosystem.
With a so called `Vagrantfile` you are able to download a machine image someone else bundled up for you and start a virtualmachine.

```bash
Vagrant.configure("2") do |config|
  config.vm.box = "generic/rocky8"
  config.vm.box_version = "3.6.8"
end
```

### Install

The [Download](https://www.vagrantup.com/downloads) page describes how to install vagrant with simple clicks and downloads. The following tabs extract the bits necessary.

=== "MacOSX"

    An easy way to install vagrant is using Homebrew.
    
    ```bash
    brew install vagrant
    ```

=== "Ubuntu/Debian"

    ```bash
    curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add - # (1)
    sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main" # (2)
    sudo apt-get update && sudo apt-get install vagrant # (3)
    ```
    1. Adding the hashicorp GPG key
    2. Adding APT repository 
    3. Update the packages from repo and install vagrant

=== "Amazon Linux"

    ```bash
    sudo yum install -y yum-utils
    sudo yum-config-manager --add-repo https://rpm.releases.hashicorp.com/AmazonLinux/hashicorp.repo
    sudo yum -y install vagrant
    ```

=== "Windows"

    Please visit the [download page](https://www.vagrantup.com/downloads) to grab the latest installer.


### Check

After you installed `vagrant`, please check the installation by running a version check.

```bash
vagrant --version
```

This should output the version

```{ .bash .no-copy}
$ vagrant --version
Vagrant 2.2.19
```

### Multi-machine Stack

Creating a single virtual machine is already nice, but there is more. You are able to create a stack of machines with defined hostnames, IP addresses etc.
We are going to use that in the HPC stack. 

``` { .bash .annotate }
Vagrant.configure("2") do |config|
  config.vm.box = "generic/rocky8"
  config.vm.box_version = "3.6.8"
  config.vm.define "node1", primary: true do |node1| # (1)
    node1.vm.network "private_network", ip: "192.168.56.101"
    node1.vm.hostname = "node1"
    node1.vm.provider "virtualbox" do |vb|
      vb.memory = 2048
      vb.cpus = 2
    end
  end
  config.vm.define "node2", primary: true do |node2| # (2)
    node2.vm.network "private_network", ip: "192.168.56.102"
    node2.vm.hostname = "node2"
    node2.vm.provider "virtualbox" do |vb|
      vb.memory = 1024
      vb.cpus = 1
    end
  end
end
```

1.  This block defines the specifics of `node1`
2.  This block defines the specifics of `node2`

