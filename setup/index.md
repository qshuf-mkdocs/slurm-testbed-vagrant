---
title: "Setup"
weight: 2
---

To create this stack we use a couple of components:

1. **Virtualbox**: Running our Virtual Machines (VM) we are going to use [VirtualBox](https://www.virtualbox.org/). It is available for most Operating Systems and it's free to use.
1. **Vagrant**: Orchestrating multiple VMs is cumbersome to do manually, so we are going to use [Vagrant]() to help us out. It also provides a convinient way of stroing virtual machine images for different providers (like virtualbox) in their cloud.
1. **Git(lab)**: The stack is stored in a gitlab repository, which we are going to clone and use.

After you completed this chapter you are going to have a running Slurm cluster on you laptop.

### The Stack

``` mermaid
classDiagram
    headnode <|-- compute1
    headnode <|-- compute2
    headnode <|-- compute3
    headnode <|-- compute4           
    headnode :  nfsd, slurmctld, slurmd
    headnode :  spack, runc,crun, sarus
    compute1 :  slurmd
    compute1 :  spack, runc,crun, sarus
    compute2 :  slurmd
    compute2 :  spack, runc,crun, sarus
    compute3 :  slurmd
    compute3 :  spack, runc,crun, sarus
    compute4 :  slurmd
    compute4 :  spack, runc,crun, sarus
```
