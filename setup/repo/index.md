---
title: "Git Repository"
weight: 3
---
The stack files are stored in the gitlab repository [qnib-vagrant/slurm-container-playground](https://gitlab.com/qnib-vagrant/slurm-container-playground).

### Install Git

=== "MacOSX"

    ```bash
    brew install git
    ```

=== "Ubuntu/Debian"

    ```bash
    apt install -y git-core
    ```


### Optional Fork

If you plan to make changes to the repo, please make a fork so that you can contribute back.

### Clone Repository

```bash
git clone https://gitlab.com/qnib-vagrant/slurm-container-playground.git 
```

