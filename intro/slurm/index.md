---
title: "SLURM"
weight: 3
---

From the [developers website](https://slurm.schedmd.com/overview.html):

> Slurm is an open source, fault-tolerant, and highly scalable cluster management and job scheduling system for large and small Linux clusters. Slurm requires no kernel modifications for its operation and is relatively self-contained. As a cluster workload manager, Slurm has three key functions. First, it allocates exclusive and/or non-exclusive access to resources (compute nodes) to users for some duration of time so they can perform work. Second, it provides a framework for starting, executing, and monitoring work (normally a parallel job) on the set of allocated nodes. Finally, it arbitrates contention for resources by managing a queue of pending work. Optional plugins can be used for accounting, advanced reservation, gang scheduling (time sharing for parallel jobs), backfill scheduling, topology optimized resource selection, resource limits by user or bank account, and sophisticated multifactor job prioritization algorithms.

Slurm is one of (the) most prominent scheduler used in HPC centers around the world. It allows to allocate resources from a homogeneous hardware *zoo* and support the usually found POSIX environment with users and software shares. 

It abstracts the computing resource away and helps end-users to utilize it without to much hassel.
