---
title: "Spack"
weight: 4
---
[Spack](https://spack.io/) is a package management tool designed to support multiple versions and configurations of software on a wide variety of platforms and environments. It was designed for large supercomputing centers, where many users and application teams share common installations of software on clusters with exotic architectures, using libraries that do not have a standard ABI. Spack is non-destructive: installing a new version does not break existing installations, so many configurations can coexist on the same system.

<iframe width="560" height="315" src="https://www.youtube.com/embed/D0p5xpsboK4" frameborder="0" allowfullscreen></iframe>
