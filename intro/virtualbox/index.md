---
title: "VirtualBox"
weight: 2
---
From the [Wikipedia article](https://en.wikipedia.org/wiki/VirtualBox): 

> Oracle VM VirtualBox (formerly Sun VirtualBox, Sun xVM VirtualBox and Innotek VirtualBox) is a [type-2 hypervisor](https://en.wikipedia.org/wiki/Type-2_hypervisor) for [x86 virtualization](https://en.wikipedia.org/wiki/X86_virtualization) developed by [Oracle Corporation](https://en.wikipedia.org/wiki/Oracle_Corporation).
>
> VirtualBox was originally created by Innotek GmbH, which was acquired by Sun Microsystems in 2008, which was in turn acquired by Oracle in 2010.
>
> VirtualBox may be installed on Microsoft Windows, macOS, Linux, Solaris and OpenSolaris. There are also ports to FreeBSD[4] and Genode.[5] It supports the creation and management of guest virtual machines running Windows, Linux, BSD, OS/2, Solaris, Haiku, and OSx86,[6] as well as limited virtualization of macOS guests on Apple hardware.[7][8] For some guest operating systems, a "Guest Additions" package of device drivers and system applications is available,[9][10] which typically improves performance, especially that of graphics, and allows changing the resolution of the guest OS automatically when the window of the virtual machine on the host OS is resized.[11]
>
> Released under the terms of the GNU General Public License and, optionally, the CDDL for most files of the source distribution, VirtualBox is free and open-source software, though the Extension Pack is proprietary software.
