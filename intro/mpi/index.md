---
title: "MPI"
weight: 5
---
From the [Wikipedia article](https://en.wikipedia.org/wiki/Message_Passing_Interface): 

> Message Passing Interface (MPI) is a standardized and portable [message-passing](https://en.wikipedia.org/wiki/Message-passing) standard designed to function on [parallel computing](https://en.wikipedia.org/wiki/Parallel_computing) architectures. The MPI standard defines the syntax and semantics of library routines that are useful to a wide range of users writing portable message-passing programs in C, C++, and Fortran. There are several open-source MPI implementations, which fostered the development of a parallel software industry, and encouraged development of portable and scalable large-scale parallel applications.

MPI is the predominant library used to communicate over High Performance Computing (HPC) interconnects. These interconnects focus on low latency and high bandwith by reliying on drivers with as little overhead as possible and a (usually) a flat network without the ususal routing.
